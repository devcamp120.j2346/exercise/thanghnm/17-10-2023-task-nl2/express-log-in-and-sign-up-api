const mongoose =require("mongoose")

const role = new mongoose.Schema({
    name:{
        type:String,
        unique:true,
        required:true
    }
},
{timestamp:true})
module.exports = mongoose.model("Role",role)