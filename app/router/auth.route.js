const express = require("express")
const { checkDublicateUsername, verifyToken } = require("../middleware/auth.middleware")
const { signUp, logIn, refreshToken } = require("../controller/auth.controlller")

const route = express.Router()

route.post("/signup",checkDublicateUsername,signUp)
route.post("/login",logIn)
route.post("/refresh/token",refreshToken)
module.exports = route