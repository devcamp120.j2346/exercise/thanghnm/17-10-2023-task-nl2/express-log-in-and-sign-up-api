const { v4: uuidv4 } = require("uuid")
const db = require("../model")
const createToken = async(user)=>{
        var expiredAt = new Date()
        expiredAt.setSeconds(
            expiredAt.getSeconds() + process.env.JWT_Refresh_Expired
        )
        // console.log(process.env.JWT_Expired)
        // console.log(expiredAt.getTime())
        let token= uuidv4()
        let refreshTokenObj = new db.refreshToken({
            token:token,
            user:user._id,
            expiredDate :expiredAt.getTime()
        })
        const refreshToken = await refreshTokenObj.save()
        return refreshToken.token
}
module.exports = { createToken}