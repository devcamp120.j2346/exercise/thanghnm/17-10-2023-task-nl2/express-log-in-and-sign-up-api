const db = require("../model")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")

const User = db.user

const checkDublicateUsername = async (req, res, next) => {
    try {
        const existUser = await User.findOne({
            username: req.body.username
        })
        if (existUser) {
            res.status(400).send({
                message: "Username is already in use"
            })
        }
        const existEmail = await User.findOne({
            email: req.body.email
        })
        if (existEmail) {
            res.status(400).send({
                message: "Email is already in use"
            })
        }
        next()
    } catch (error) {
        console.error("Internal server error", error)
        process.exit()
    }
}
const verifyToken = async (req, res, next) => {
    try {
        console.log("verify token...")
        const token = req.headers.authorization.split(" ")[1]
        // console.log(token)
        if (!token) {
            return res.status(404).json({
                message: "Token not found"
            })
        }
        const secretKey = process.env.JWT_Secret
        const verified = await jwt.verify(token, secretKey)
        if (!verified) {
            return res.status(404).json({
                message: "Token không hợp lệ"
            })
        }
        const user = await User.findById(verified.id).populate("role")
        // console.log(user)
        req.user = user
        next()
    } catch (error) {
        console.error("Internal server error", error)
        process.exit()
    }
}
const checkUser = async (req, res, next) => {
    console.log("check user ...")
    const userRoles = req.user.roles;
    if (userRoles) {
        for (let i = 0; i < userRoles.length; i++) {
            if (userRoles[i].name == 'admin') {
                console.log('authorized!');
                next();
                return;
            }
        }
    }
    console.log('unauthorized!');
    return res.status(401).json({
        message: "Unauthorized!"
    })

}
module.exports = { checkDublicateUsername, verifyToken,checkUser }