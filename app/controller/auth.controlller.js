const db = require("../model")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const refreshTokenService = require("../services/refreshToken.service")
const signUp = async (req, res) => {
    try {

        const role = await db.role.findOne({
            name: "user"
        })
        const user = new db.user({
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password, 5),
            email: req.body.email,
            role: role._id
        })

        if (!user.password) {
            return res.status(404).json({
                message: "password not found"
            })
        }
        if (!user.email) {
            return res.status(404).json({
                message: "email not found"
            })
        }
        await user.save()
        return res.status(201).json({
            message: "Create user successfully"
        })
    } catch (error) {
        res.status(500).json({
            message: "Internal servel error"
        })
    }

}
const logIn = async (req, res) => {
    try {
        const existUser = await db.user.findOne({
            email: req.body.email
        })
        if (!existUser) {
            return res.status(404).json({
                message: "User not found"
            })
        }
        var passwordIsValid = bcrypt.compareSync(
            req.body.password,
            existUser.password
        )
        if (!passwordIsValid) {
            res.status(400).json({
                message: "Password Invalid"
            })
        }
        const secretKey = process.env.JWT_Secret
        const token = jwt.sign({
            id: existUser._id,
        },
            secretKey,
            {
                algorithm: "HS256",
                allowInsecureKeySizes: true,
                expiresIn: process.env.JWT_Access_Expired
            }
        )
        //Sinh thêm refresh token
        const refreshToken = await refreshTokenService.createToken(existUser)
        return res.status(200).json({
            accessToken: token,
            refreshToken: refreshToken
        })
    } catch (error) {
        console.log(error)
        res.status(500).json({
            message: "Internal servel error",
            
        })
    }
}
const refreshToken = async (req, res) => {
    const refreshToken = req.body.refreshToken
    // console.log(refreshToken)
    if (!refreshToken) {
        return res.status(403).json({
            message: " Request Token is required"
        })
    }
    try {
        const refreshTokenObj = await db.refreshToken.findOne({
            token: refreshToken
        })
        if (!refreshTokenObj) {
            return res.status(403).json({
                message: "Request token not found"
            })
        }
        // Nếu req token hết hạn
        if (refreshTokenObj.expiredDate.getTime() < new Date().getTime()) {
            // xóa req token trong csdl
            await db.refreshToken.findByIdAndRemove(refreshTokenObj._id)
            return res.status(403).json({
                message: " Request Token was expired!"
            })
        }
        const secretKey = process.env.JWT_Secret

        const newAccessToken = jwt.sign({
            id: refreshTokenObj.user,
        },
            secretKey,
            {
                algorithm: "HS256",
                allowInsecureKeySizes: true,
                expiresIn: process.env.JWT_Access_Expired
            }
        )
        res.status(200).json({
            accessToken: newAccessToken,
            refreshToken: refreshTokenObj.token
        })

    } catch (error) {
        res.status(500).json({
            message: "Internal servel error",
            error
        })
    }
}
//
module.exports = { signUp, logIn, refreshToken }