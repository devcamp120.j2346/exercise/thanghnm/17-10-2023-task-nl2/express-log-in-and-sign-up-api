const db = require("../model")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const createProduct = async(req,res)=>{
    try {
        const {name,price} = req.body
        var vProduct = {
            name,
            price
        }
       const createProduct =  await  db.product.create(vProduct)
        return res.status(201).json({
            message:"Create Product successfully"
        })
    } catch (error) {
        res.status(500).json({
            message:"Internal server error"
        })
    }
}
const getAllProDuct = async(req,res)=>{
    try {

        const getAllProDuct = await db.product.find()
        return res.status(200).json({
            result:getAllProDuct
        })
    } catch (error) {
        res.status(500).json({
            message:"Internal server error"
        }) 
    }
}
const getProductById = async(req,res)=>{
    try {
        const productId = req.params.productId
        const getProductById = await db.product.findById(productId)
        return res.status(200).json({
            result:getProductById
        })
    } catch (error) {
        res.status(500).json({
            message:"Internal server error"
        })  
    }
}
const updateProductById = async(req,res)=>{
    try {
        const {name,price} = req.body
        const productId = req.params.productId
        var vUpdate = {
            name,
            price
        }
        const updateProductById = await db.product.findByIdAndUpdate(productId)
        return res.status(200).json({
            result:updateProductById
        })
    } catch (error) {
        res.status(500).json({
            message:"Internal server error"
        })  
    }
}
 const deleteProductById = async (req,res)=>{
    try {
        const productId = req.params.productId
        const deleteProductById = await db.product.findByIdAndDelete(productId)
        return res.status(200).json({
            message:"Delete product successfully"
        })
    } catch (error) {
        res.status(500).json({
            message:"Internal server error"
        })    
    }
 }
module.exports = {
    createProduct,getAllProDuct, getProductById,updateProductById,deleteProductById
}