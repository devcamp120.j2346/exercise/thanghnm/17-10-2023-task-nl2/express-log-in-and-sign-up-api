const express = require("express")
const app = new express()
const port = 8000
const path = require("path")
const mongoose = require("mongoose")
const db = require("./app/model")
require("dotenv").config()
app.use(express.json())
// Hiển thị hình ảnh
app.use(express.static(__dirname + "/views"))
mongoose.connect("mongodb://127.0.0.1:27017/Auth")
.then(()=>console.log("Connected to Mongo Successfully"))
.catch(error=>handleEror(error))
// Router 
app.get("/",(req, res) => {
    res.sendFile(path.join(__dirname + "/views/login.html"))
})
app.get("/signup/",(req, res) => {
    res.sendFile(path.join(__dirname + "/views/signup.html"))
})
app.use("/api/auth",require("./app/router/auth.route"))
app.use("/api/product",require("./app/router/product.route"))
//
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})